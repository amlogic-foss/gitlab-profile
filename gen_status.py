import os
import sys
import argparse
import json
from datetime import datetime

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("json", help="pipeline json file")
    ap.add_argument("markdown", help="markdown template file")
    ap.add_argument("tag", help="markdown tag to replace")
    ap.add_argument("max", help="max entries")
    ap.add_argument("out", help="output markdown file")
    args = ap.parse_args()

    with open(args.json, 'r', encoding='utf-8') as f:
        status = json.loads(f.read())

    with open(args.markdown, 'r', encoding='utf-8') as f:
        template = f.read()

    pipeline_status = "| Date | Tag | Status |\n| --- | --- | --- |\n"
    count = 0

    for pipeline in status:
        name = pipeline["name"]
        status = pipeline["status"]
        url = pipeline["web_url"]
        date = datetime.fromisoformat(pipeline["updated_at"].split(".")[0])
    
        if status in ["manual", "canceled", "skipped"]:
            continue

        if status in ["success"]:
            line = "| %s | [%s](%s) | :white_check_mark: |\n" % (date.strftime("%c"), name, url)
        elif status in ["failed"]:
            line = "| %s | [%s](%s) | :red_circle: |\n" % (date.strftime("%c"), name, url)
        else:
            line = "| %s | [%s](%s) | :large_blue_circle: |\n" % (date.strftime("%c"), name, url)

        pipeline_status = pipeline_status + line
        count = count + 1

        if count > int(args.max):
            break

    template = template.replace(args.tag, pipeline_status)

    with open(args.out, 'w+', encoding='utf-8') as f:
        f.write(template)