# Amlogic CI repositories for U-Boot and Linux

## Task Tracker

Some tasks are for Mainline U-Boot and Linux are tracked in the following board:
https://gitlab.com/groups/amlogic-foss/-/boards/7170061

The projects used to track are:
- Linux: https://gitlab.com/amlogic-foss/mainline-linux-issues-tracker
- U-Boot: https://gitlab.com/amlogic-foss/mainline-u-boot-issues-tracker

## Automomous CI System

### Linux Pipeline Status

LINUX_PIPELINE_STATUS

[More...](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines)

### U-Boot Pipeline Status

UBOOT_PIPELINE_STATUS

[More...](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines)

![Amlogic CI diagram](amlogic-foss-ci.png)

### U-Boot

U-boot master is build every week, and u-boot-amlogic custodian is built on push events.

U-boot is packaged into FIP and tested on real hardware with the https://github.com/starnux/abcd tool.

Pipelines are available at:
https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines

### Linux

Linux requires more components to be tested, so we need:
- initramfs
- dtbs
- modules
- Image

The initramfs is weekly built from Yocto and meta-meson master from https://gitlab.com/amlogic-foss/meta-meson-ci,
initramfs can be found in https://gitlab.com/amlogic-foss/meta-meson-ci/-/packages

The Linux dtbs, modules & Image are build from https://gitlab.com/amlogic-foss/linux-amlogic-ci push events
from weekly Syncs from linux-next, linux-amlogic and linux-stable tags.
The kernel binaries can be found at https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/packages/

The sync pipeline and Linux build pipeline are stored at https://gitlab.com/amlogic-foss/linux-amlogic-ci-pipeline

When kernel is built, the last initramfs & the last u-boot binary will be used to boot the kernel
with the https://github.com/starnux/abcd tool and run tests present on the initramfs.

The full test pipepines with latest U-Boot Master, poky/meta-meson and kernels can be found at:
https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines

Each tag pushed into https://gitlab.com/amlogic-foss/linux-amlogic-ci are built and then tested with
the pipeline defined in https://gitlab.com/amlogic-foss/abcd-linux-test as multi-project pipeline.

### Next Steps

Implement a test suite in https://gitlab.com/amlogic-foss/meta-meson-ci to got beyond boot testing.

## About

For more informations about the Linux and U-Boot upstream status see http://linux-meson.com