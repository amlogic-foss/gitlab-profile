# Amlogic CI repositories for U-Boot and Linux

## Task Tracker

Some tasks are for Mainline U-Boot and Linux are tracked in the following board:
https://gitlab.com/groups/amlogic-foss/-/boards/7170061

The projects used to track are:
- Linux: https://gitlab.com/amlogic-foss/mainline-linux-issues-tracker
- U-Boot: https://gitlab.com/amlogic-foss/mainline-u-boot-issues-tracker

## Automomous CI System

### Linux Pipeline Status

| Date | Tag | Status |
| --- | --- | --- |
| Thu Mar  6 11:36:06 2025 | [Linux Pipeline for: next-20250306](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines/1702808857) | :red_circle: |
| Wed Mar  5 12:14:35 2025 | [Linux Pipeline for: next-20250305](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines/1700856839) | :white_check_mark: |
| Tue Mar  4 12:07:10 2025 | [Linux Pipeline for: next-20250304](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines/1698808829) | :white_check_mark: |
| Mon Mar  3 11:47:31 2025 | [Linux Pipeline for: v6.14-rc5](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines/1696676314) | :white_check_mark: |
| Mon Mar  3 12:03:47 2025 | [Linux Pipeline for: next-20250303](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines/1696676260) | :white_check_mark: |
| Sun Mar  2 20:03:26 2025 | [Linux Pipeline for: master](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines/1695849659) | :white_check_mark: |


[More...](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines)

### U-Boot Pipeline Status

| Date | Tag | Status |
| --- | --- | --- |
| Thu Mar  6 08:53:00 2025 | [U-Boot Autotest for "master"](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines/1702478256) | :red_circle: |
| Wed Mar  5 08:25:05 2025 | [U-Boot Autotest for "master"](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines/1700513904) | :white_check_mark: |
| Tue Mar  4 08:25:40 2025 | [U-Boot Autotest for "master"](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines/1698383857) | :white_check_mark: |
| Mon Mar  3 08:25:06 2025 | [U-Boot Autotest for "master"](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines/1696364549) | :white_check_mark: |
| Sun Mar  2 08:24:54 2025 | [U-Boot Autotest for "master"](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines/1695186355) | :white_check_mark: |
| Sat Mar  1 08:25:09 2025 | [U-Boot Autotest for "master"](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines/1694492230) | :white_check_mark: |


[More...](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines)

![Amlogic CI diagram](amlogic-foss-ci.png)

### U-Boot

U-boot master is build every week, and u-boot-amlogic custodian is built on push events.

U-boot is packaged into FIP and tested on real hardware with the https://github.com/starnux/abcd tool.

Pipelines are available at:
https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines

### Linux

Linux requires more components to be tested, so we need:
- initramfs
- dtbs
- modules
- Image

The initramfs is weekly built from Yocto and meta-meson master from https://gitlab.com/amlogic-foss/meta-meson-ci,
initramfs can be found in https://gitlab.com/amlogic-foss/meta-meson-ci/-/packages

The Linux dtbs, modules & Image are build from https://gitlab.com/amlogic-foss/linux-amlogic-ci push events
from weekly Syncs from linux-next, linux-amlogic and linux-stable tags.
The kernel binaries can be found at https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/packages/

The sync pipeline and Linux build pipeline are stored at https://gitlab.com/amlogic-foss/linux-amlogic-ci-pipeline

When kernel is built, the last initramfs & the last u-boot binary will be used to boot the kernel
with the https://github.com/starnux/abcd tool and run tests present on the initramfs.

The full test pipepines with latest U-Boot Master, poky/meta-meson and kernels can be found at:
https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines

Each tag pushed into https://gitlab.com/amlogic-foss/linux-amlogic-ci are built and then tested with
the pipeline defined in https://gitlab.com/amlogic-foss/abcd-linux-test as multi-project pipeline.

### Next Steps

Implement a test suite in https://gitlab.com/amlogic-foss/meta-meson-ci to got beyond boot testing.

## About

For more informations about the Linux and U-Boot upstream status see http://linux-meson.com