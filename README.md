# Amlogic CI repositories for U-Boot and Linux

## Task Tracker

Some tasks are for Mainline U-Boot and Linux are tracked in the following board:
https://gitlab.com/groups/amlogic-foss/-/boards/7170061

The projects used to track are:
- Linux: https://gitlab.com/amlogic-foss/mainline-linux-issues-tracker
- U-Boot: https://gitlab.com/amlogic-foss/mainline-u-boot-issues-tracker

## Automomous CI System

### Linux Pipeline Status

| Date | Tag | Status |
| --- | --- | --- |
| Mon Mar 10 11:42:32 2025 | [Linux Pipeline for: v6.14-rc6](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines/1708135134) | :white_check_mark: |
| Sun Mar  9 20:03:06 2025 | [Linux Pipeline for: master](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines/1707324400) | :white_check_mark: |
| Sun Mar  9 11:39:42 2025 | [Linux Pipeline for: v6.6.82](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines/1707079139) | :white_check_mark: |
| Sat Mar  8 00:11:45 2025 | [Linux Pipeline for: v6.6.81](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines/1705721921) | :white_check_mark: |
| Sat Mar  8 00:00:11 2025 | [Linux Pipeline for: v6.13.6](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines/1705721569) | :white_check_mark: |
| Fri Mar  7 23:49:51 2025 | [Linux Pipeline for: v6.12.18](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines/1705721182) | :white_check_mark: |


[More...](https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines)

### U-Boot Pipeline Status

| Date | Tag | Status |
| --- | --- | --- |
| Mon Mar 10 08:25:03 2025 | [U-Boot Autotest for "master"](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines/1707819638) | :white_check_mark: |
| Sun Mar  9 08:25:14 2025 | [U-Boot Autotest for "master"](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines/1706984335) | :white_check_mark: |
| Sat Mar  8 08:26:36 2025 | [U-Boot Autotest for "master"](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines/1706110285) | :white_check_mark: |
| Fri Mar  7 11:01:44 2025 | [U-Boot Autotest for "master"](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines/1704418930) | :white_check_mark: |
| Fri Mar  7 10:34:17 2025 | [U-Boot Autotest for "master"](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines/1702478256) | :white_check_mark: |
| Wed Mar  5 08:25:05 2025 | [U-Boot Autotest for "master"](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines/1700513904) | :white_check_mark: |


[More...](https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines)

![Amlogic CI diagram](amlogic-foss-ci.png)

### U-Boot

U-boot master is build every week, and u-boot-amlogic custodian is built on push events.

U-boot is packaged into FIP and tested on real hardware with the https://github.com/starnux/abcd tool.

Pipelines are available at:
https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/pipelines

### Linux

Linux requires more components to be tested, so we need:
- initramfs
- dtbs
- modules
- Image

The initramfs is weekly built from Yocto and meta-meson master from https://gitlab.com/amlogic-foss/meta-meson-ci,
initramfs can be found in https://gitlab.com/amlogic-foss/meta-meson-ci/-/packages

The Linux dtbs, modules & Image are build from https://gitlab.com/amlogic-foss/linux-amlogic-ci push events
from weekly Syncs from linux-next, linux-amlogic and linux-stable tags.
The kernel binaries can be found at https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/packages/

The sync pipeline and Linux build pipeline are stored at https://gitlab.com/amlogic-foss/linux-amlogic-ci-pipeline

When kernel is built, the last initramfs & the last u-boot binary will be used to boot the kernel
with the https://github.com/starnux/abcd tool and run tests present on the initramfs.

The full test pipepines with latest U-Boot Master, poky/meta-meson and kernels can be found at:
https://gitlab.com/amlogic-foss/linux-amlogic-ci/-/pipelines

Each tag pushed into https://gitlab.com/amlogic-foss/linux-amlogic-ci are built and then tested with
the pipeline defined in https://gitlab.com/amlogic-foss/abcd-linux-test as multi-project pipeline.

### Next Steps

Implement a test suite in https://gitlab.com/amlogic-foss/meta-meson-ci to got beyond boot testing.

## About

For more informations about the Linux and U-Boot upstream status see http://linux-meson.com